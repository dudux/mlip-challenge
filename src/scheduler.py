'''
Created on 7 Apr 2014

@author: Carlo Meijer
'''

from numpy import *
from pylab import *
import numpy as np
#import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn import svm
from sklearn import cross_validation
from copy import deepcopy
import matplotlib as plt
from toolbox_02450 import rocplot, confmatplot
import sys
#from scipy.stats import norm
import scipy


from dataReader import *
from dataTransformation import transformToClassification
from saveLoad import *
from models import *
from MLiP_eval import StrategyScheduleScore

schedulerData = None

class SchedulerData:
    def __init__(self):
        self.clfs, self.clerrors, self.configs, self.performances = loadClassifier('mlClassifier1.clf')
        #self.models, self.errors = loadClassifier('linregModel.mod')
        self.models, self.errors = loadClassifier('DecisionTreeModel1.mod')
        self.correlation = loadClassifier(filename='strategies1.cor')
        self.strategyNames = loadStrategyNames()

    @staticmethod
    def getInstance():
        global schedulerData
        if schedulerData == None:
            schedulerData = SchedulerData()
        return schedulerData

def scheduler(data, alpha=.6, beta=10., gamma=3):
# alpha :: probability percentage of running time estimate
# beta  :: weight assigned to the probability a strategy will get done
# gamma :: weight assigned to penalty given to strategy with high correlation already scheduled

    # step 0: initialize
    
    sdata = SchedulerData.getInstance()
    X = np.matrix([problem[1] for problem in data])

    strategyNames = loadStrategyNames()
    nStrategies = len(sdata.strategyNames)
  #  (clfs, errors, configs, performances) = multiLinearClassificationImproved(trainData)
    #(clfs, clerrors, configs, performances) = loadClassifier('mlClassifier.clf')
   # saveClassfier((clfs, errors, configs, performances),filename='mlClassifier.clf')
    #(models, errors) = linearRegressionModel(trainData)
    #saveClassifier((models,errors), filename = "linregModel.mod")
    #(models, errors) = loadClassifier('linregModel.mod')
    
    #correlation = pearsonCorrelation(transformedData)
    #saveClassifier(correlation,filename="strategies.cor")
    #correlation = loadClassifier(filename='strategies.cor')
    #A = int(float(len(trainData)) * .5)
    alphaVal = scipy.stats.norm.ppf(alpha)

    minusOneProbList = []
    runningTimeList  = []
    #initScoreList    = []

    for i in xrange(nStrategies):
        # step 1: classify
        tmp = sdata.clfs[i].predict_proba(X)
        pred = [asd[1] for asd in tmp]
        minusOneProbList.append(pred)

        # step 2: determine strategy running time
        pred = sdata.models[i].predict(X)
        # cannot allocate less-than zero time
        corrected = map(lambda x: max(1., x), pred)
        # correct predicted running time with stddev * ppf(alpha)
        runningTimeEntry = map(lambda x: sqrt(sdata.errors[i]) * alphaVal + x, corrected)
        # initial score values do allow for netative values since they only indicate the priority
        #initScoreEntry   = map(lambda x: sqrt(sdata.errors[i]) * alphaVal + x, pred)
        runningTimeList.append(runningTimeEntry)
        #runningTimeList.append(corrected)
        #initScoreList.append(initScoreEntry)

    minusOneProb = np.matrix(minusOneProbList) # matrix of probabilities the strategy will yield -1
    runningTime  = np.matrix(runningTimeList)  # matrix of expected running times
    ##initScore    = np.matrix(initScoreList)    # initial score matrix - will be adjusted later

    output = []
    # iterate the input
    for i in xrange(len(data)):
        # extract rows from the matrix that are applicable to this row
        minusOneProbCurrent = minusOneProb[:,i].A.ravel().tolist()
        runningTimeCurrent  = runningTime[:,i].A.ravel().tolist()
        #initScoreCurrent    = initScore[:,i].A.ravel().tolist()

        scores = []
        for j in xrange(nStrategies):
            #score = initScoreCurrent[j]
            # carlicious: no more scheduling based on initScore since this may prioritize jobs with extremely high stddevs
            score = runningTimeCurrent[j]
            # avoid division by zero
            if minusOneProbCurrent[j] == 1.0:
                score = float("infinity")
            else:
                # update score by the probability the strategy yields -1
                score /= (beta * (1. - minusOneProbCurrent[j]))
            scores.append((j, score))

        schedule = []
        # sort scores in ascending order - lower is better
        scores = sorted(scores, cmp=lambda x,y: cmp(x[1], y[1]))

        timeAllocated = 0.
        while True:
            # save index of best strategy
            index = scores[0][0]
            # round the required time to a single decimal
            timeNeeded = float(int(np.round(min(300 - timeAllocated, runningTimeCurrent[index]) * 10))) / 10.
            # take best strategy
            schedule.append((scores[0], timeNeeded))
            # update allocated time
            timeAllocated += timeNeeded

            if timeAllocated >= 299.99 or len(scores) == 1:
                break
            # remove strategy from queue
            scores = scores[1:]
            # update queue with correlation penalty
            for j in xrange(len(scores)):
                scores[j] = (scores[j][0], scores[j][1] + gamma * sdata.correlation[index, scores[j][0]])

            # order of queue may have changed - sort queue again
            scores = sorted(scores, cmp=lambda x,y: cmp(x[1], y[1]))

        #print schedule
        line = ''
        for job in schedule:
            if len(line) != 0:
                line += ','
            line += sdata.strategyNames[job[0][0]] + ':' + str(job[1])

        line = data[i][0] + '#' + line

        #print line
        output.append(line)
        #if i == 10:
            #break
    return output