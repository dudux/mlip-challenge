'''
Created on 1 Apr 2014

@author: Stef Janssen
'''

#### IMPORT OUR OWN MODULES HERE
from dataReader import loadData
from dataTransformation import doPCA
from dataTransformation import transformToClassification
from dataTransformation import normalizeData
from dataTransformation import removeOutliers
from models import *
from saveLoad import *
from ParameterEstimation import runEstimation
from scheduler import *


### IMPORT OTHER MODULES HERE
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
from pylab import *
from sklearn.grid_search import GridSearchCV
import sys
import ast


###


### FOR SVM NORMALIZED DATA IS NEEDED
def trainRegressor(regressionData, leaveOneOut = True):
    (clfs, errors, means, stddevs) = svmMultiLinearRegression(regressionData, leaveOneOut)  
    plot(errors)
    print mean(errors)
    print errors
    saveClassifier((clfs, errors, means, stddevs), filename = 'svmRegression.clf')
    return clfs, errors, means, stddevs

### returns indices of strategies which should be able to solve the problem
def predictWorkingStrategies(clfs, feature):
    strategyIndices = []
    for i in range(len(clfs)):
        if clfs[i].predict(feature): #can solve
            strategyIndices.append(i)
    return strategyIndices
    

def trainClassifier(classificationData):
    clfs, errors = multiLinearClassification(classificationData)
    plot(errors)
    saveClassifier((clfs, errors), 'svmClassification.clf')
    print mean(errors)
    return clfs, errors

def testHyperParameters(classificationData):
    mean_errors = []
    #for i in range(5, 10):
    #    clfs, errors = multiLinearClassification(classificationData, _max_depth = i)
    #    print 'For max_depth of {0} we got a mean error of {1}'.format(i, mean(errors))
    #    mean_errors.append(mean(errors))
    #plot(mean_errors)
    for i in range(5, 10):
        clfs, errors = multiLinearClassification(classificationData, _min_samples_split = i, _max_depth = 7)
        print 'For min_samples_split of {0} we got a mean error of {1}'.format(i, mean(errors))
        mean_errors.append(mean(errors))
    plot(mean_errors)
    


def doMyPCA(trainData, visualize = False):
    Z = doPCA(trainData, visualize)
    print 'Done with PCA'
    return Z

def evaluate(Eval, sched):
    score = 0.0
    solved = 0.0
    total = 0.0
    for line in sched:
        total += 1
        pScore = Eval.eval_schedule(line)
        score += pScore
        if pScore > 0:
            solved += 1
    solvedPercentage = round(100*solved/total,2)
    avg = (solvedPercentage + 100*score/300)/2.
    return score, solvedPercentage, avg

def myRange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step
    	
def usage():
    print "%s True/False" %(sys.argv[0])
    print "\t\tTrue  --> Train"
    print "\t\tFalse --> not Train"
    sys.exit()



if len(sys.argv)==1:
    usage()
    sys.exit()
elif (str(sys.argv[1])=='True'):    # Train = True
    print 'Starting training..'
    # =========== still to be done on 32bit platform ==========
    trainData = loadData()   ##Default path works
    print 'removing outliers'
    trainData = removeOutliers(trainData)
    with open('trainData', 'w') as f:
      f.write(repr(trainData))   
    f.close()
    transformedData = transformToClassification(trainData)
    print 'outliers successfully removed : {0}'.format(900 - len(trainData))
    print 'training classifier'
    (clfs, errors, configs, performances) = multiLinearClassificationImproved(transformedData)
    saveClassifier((clfs, errors, configs, performances),filename='mlClassifier1.clf')
    print 'training regressor'
    (models, errors) = decisionTreeModel(trainData)
    saveClassifier((models,errors), filename = "decisionTreeModel1.mod")
    print 'computing correlations'
    correlation = pearsonCorrelation(transformedData)
    saveClassifier(correlation,filename="strategies1.cor")
        
    print 'training SVM regressor'
    (models, errors) = decisionTreeModel(trainData)
    saveClassifier((models,errors), filename = "SVMModel1.mod")
else:
    print 'Starting'
    trainData = []
    with open('trainData', 'r') as f:
       s = f.read()
    f.close()
    trainData = ast.literal_eval('{0}'.format(s))

   
#saved files with 1 after the name are with outlier removal    

#To compare differences outside the parameters. Change something in the program and run this
#to see how well it does.
 
    
# ========================================================

#scheduler(trainData)

#clfs2, errors2, means2, stddevs2 = loadClassifier('svmRegression.clf')
#clfs3, errors3 = loadClassifier('svmClassification.clf')

#saveClassifier((clfs,errors,performances),filename='DecisionTree.clf')
#(clfs,errors,performances) = loadClassifier('DecisionTree.clf')




Eval = StrategyScheduleScore('../Data/MLiP_train')

bestSolvedPercentage = None
bestSolvedPercentageGamma = 0
bestScore = None
bestScoreGamma = 0
bestAvg = None
bestAvgGamma = 0
#beta=8, alpha=0.5, gamma=5
sched = scheduler(trainData, alpha=0.5, beta=8, gamma=float(5))
score,solvedPercentage,avg = evaluate(Eval,sched) 

'''
for gamma in range (1,20):
    print 'trying gamma={0}'.format(gamma)
    
    
    sched = scheduler(trainData, alpha=0.5, beta=8, gamma=float(gamma))
    score,solvedPercentage,avg = evaluate(Eval,sched) 

    if bestSolvedPercentage == None or bestSolvedPercentage < solvedPercentage:
        bestSolvedPercentageGamma = gamma
        bestSolvedPercentage = solvedPercentage
    if bestScore == None or bestScore < score:
        bestScoreGamma = gamma
        bestScore = score
    if bestAvg == None or bestAvg < avg:
        bestAvgGamma = gamma
        bestAvg = avg
'''
print 'best solved percentage : {0}'.format(solvedPercentage)
#print '                  gamma : {0}'.format(bestSolvedPercentageGamma)
print 'best score             : {0}'.format(score)
#print '                  gamma : {0}'.format(bestScoreGamma)
print 'best average           : {0}'.format(avg)
#print '                  gamma : {0}'.format(bestAvgGamma)






#show()

sys.exit()

#scheduler(trainData, alpha=.55, beta=5., gamma=5.)
#testData = loadData(train = False)                      ##Same as trainset, except the third variable is missing (strategy training times)     

#newData = removeOutliers(trainData)
#print 'Length new data: {0}'.format(eln(newData))
help(DecisionTreeClassifier)
classificationData = normalizeData(transformToClassification(trainData))  ##Changes to classification problem



#newClassificationData = normalizeData(transformToClassification(newData)) 

#regressionData = normalizeData(trainData)

#trainRegressor(regressionData, leaveOneOut = False)

#Z = doMyPCA(trainData, visualize = True)                              ##Performs PCA

#testHyperParameters(classificationData)
#clfs, errors, performances = multiLinearClassification(classificationData, _min_samples_split = 5, _max_depth = 7)
#saveClassifier((clfs,errors,performances),filename='DecisionTree.clf')

(clfs,errors,performances) = loadClassifier('DecisionTree1.clf')

#sorts on the error
tuples = [(errors[i], performances[i]) for i in range(len(errors))]

s = sorted(tuples, cmp=lambda x,y: cmp(x[0], y[0]))

print s
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(s)
plt.xlabel('strategy')
plt.ylabel('error(blue) and performance(green)')
plt.show()

tuples = strategyPruning(tuples)

#trainClassifier(newClassificationData)
#runEstimation(classificationData)

clfs2, errors2, means2, stddevs2 = loadClassifier('svmRegression.clf')
clfs3, errors3 = loadClassifier('svmClassification.clf')


if False:
    
    print 'Classifier error rate: ' + str(errors3)
    print mean(errors2)
    print errors2
    a = errors2.index(min(errors2))
    print a
    print stddevs2[a]
    
k=0



if False:
    Z = doPCA(trainData)
    fig = plt.figure(k)
    ax = fig.add_subplot(111)
    for p in Z:
        print p[0]
    ax.scatter([problem[0,0] for problem in Z], [problem[0,1] for problem in Z])
    xlabel('feature: ' + str(0))
    ylabel('feature: ' + str(1))
    title('PCA data: first 2 features')

if False:
    for i in range(len(regressionData[0][1])-1):
        k+=1
        fig = plt.figure(k)
        ax = fig.add_subplot(111)
        ax.scatter([problem[1][i] for problem in regressionData], [problem[1][i+1] for problem in regressionData])
        xlabel('feature: ' + str(i))
        ylabel('feature: ' + str(i+1))