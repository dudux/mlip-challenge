'''
Created on 1 Apr 2014



@author: Stef Janssen
'''

'''
Reads in data, set train to true for a train set and to false for a test set. (test set only excludes the training times from the returned 
data set)
'''

def loadStrategyNames(filename = '../Data/MLiP_train'):
    output = {}
    with open(filename,'r') as IS:
        for line in IS:
            tmp = line.split('#')[-1].strip()
            tmp = tmp.split(',')
            for i,n in enumerate(tmp):
                output[i] = n

            break

    return output

def loadData(filename = '../Data/MLiP_train', train = True):
    # load the data with or without search strategy times
    trainData = []
    trainDataName = filename
    
    with open(trainDataName,'r') as IS:
        firstLine = True
        for line in IS:
            if firstLine:
                firstLine = False
                continue
            tmp = (line.strip()).split('#')
            pName = tmp[0]
            pFeatures = [float(x) for x in tmp[1].split(',')]
            if train:
                strategyTimes = [float(x) for x in tmp[2].split(',')]
                trainData.append((pName,pFeatures, strategyTimes))
            else:
                trainData.append((pName, pFeatures))
                
    return trainData