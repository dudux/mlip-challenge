'''
Created on 1 Apr 2014



@author: Stef Janssen
'''

from numpy import *
import numpy as np
import matplotlib.pyplot as plt
from scipy import linalg
from copy import deepcopy

from iForest import iForest

##### Transform the data such that all solved problems are 1's and all not solved are -1's
def transformToClassification(trainData):
    transformedData = []
    
    for problem in trainData:
        strategyTimes = deepcopy(problem[2])
        for i in xrange(len(strategyTimes)):
            if strategyTimes[i] != -1:
                strategyTimes[i] = 1
            else:
                strategyTimes[i] = 0
        transformedData.append((problem[0], problem[1], strategyTimes))
    
    return transformedData

def removeOutliers(trainData):
    data = np.matrix([problem[1] for problem in trainData])
    ifor = iForest(data)
    output = []
    for i in xrange(data.shape[0]):
        point = [data[i,j] for j in xrange(data.shape[1])]
        score = ifor.anomalyScore(point)

        if score < .68:
            output.append(trainData[i])

    return output

def normalizeData(trainData):
    print 'normalizing'
    
    normalizedData = []
    matrix = [trainData[i][1] for i in range(len(trainData))]
    normalizedFeatures = np.asmatrix(matrix)
    
    
    my_mean = mean(normalizedFeatures)
    my_stddev = std(normalizedFeatures)
    
    normalizedFeatures = (normalizedFeatures - my_mean) / my_stddev
    
    for i in range(len(trainData)):
        normalizedData.append(  (trainData[i][0], squeeze(np.asarray(normalizedFeatures[i,:])), trainData[i][2])  )
    
    print 'done normalizing'
   
    return normalizedData

###Does PCA and returns U, s, V
def doPCA(trainData, visualize = False):
   
    features = []
    for problem in trainData:
        features.append(problem[1])
    
    nrows, ncols = shape(features)
    print nrows
    print ncols
    features = np.asarray(features)

    means = []
    for col in range(ncols):
        means.append(mean(features[:, col]))

    ONE = np.asmatrix([[1] for row in range(nrows)])

    #calc zero mean data
    features = features - ONE * means
    
    SVD = linalg.svd(np.asmatrix(features))
    
    U, s, V = SVD
    
    #calculate the sum of the diagonal
    diagonal_sum = 0
    for sm in s:
        diagonal_sum += pow(sm, 2)
        
    PCA_comps = []
    for sm in s:
        PCA_comps.append(pow(sm, 2)/diagonal_sum)
    
    if visualize:
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111)
        ax1.plot(PCA_comps) 
        
        #Verify first 3 attributes are more than 90% of variation:
        print '\nSum of variation of the first 3 attributes:'
        print sum(PCA_comps[:3])
        
    Z = features * V
    return Z


