'''
Created on 4 Apr 2014

@author: Stef Janssen
'''

if __name__ == '__main__':
    pass

# exercise 8.1.1

from pylab import *
from scipy.io import loadmat
from sklearn import cross_validation
from sklearn.linear_model import LogisticRegression
from toolbox_02450 import rocplot, confmatplot

close('all')

# read data
mat_data = loadmat('../testdata/wine2.mat')
X = np.matrix(mat_data['X'])
y = np.matrix(mat_data['y'])
attributeNames = [name[0] for name in mat_data['attributeNames'].squeeze()]
classNames = [name[0] for name in mat_data['classNames'].squeeze()]
N, M = X.shape
print N, M
C = len(classNames)

# K-fold crossvalidation
K = 2
CV = cross_validation.StratifiedKFold(y.A.ravel().tolist(),K)

#X = X[:,-1]    uncomment to only choose alcohol as predicting attribute


k=0
for train_index, test_index in CV:

    # extract training and test set for current CV fold
    X_train, y_train = X[train_index,:], y[train_index,:]
    X_test, y_test = X[test_index,:], y[test_index,:]

    logit_classifier = LogisticRegression()
    logit_classifier.fit(X_train, y_train.A.ravel())

    y_test_est = np.mat(logit_classifier.predict(X_test)).T
    p = np.mat(logit_classifier.predict_proba(X_test)[:,1]).T

    figure(k)
    rocplot(np.asarray(p), y_test)

    figure(k+1)
    confmatplot(np.asarray(y_test),np.asarray(y_test_est))

    k+=2
    
show()   


#Ex811
#The plots show that the AUC is around 0.99

#Ex812
#The ROC and AUC look way worse. The ROC curve shows that the true positive rate is pretty much the 
#same as the false positive rate, which means it's just kind of guessing. The AUC is slightly higher
#0.5 so it performs ever so slightly better than just guessing in term of identifying positives.
































