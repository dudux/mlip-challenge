'''
Created on 7 Apr 2014

@author: Stef Janssen
'''

from sklearn.externals import joblib
import platform

svmFileName = 'svm.clf'
nnFileName = 'ann.clf'

def saveClassifier(attributes, filename = svmFileName):
    dot = filename.rfind('.')
    if dot == -1:
        dot = len(filename)
    if platform.architecture()[0] == '64bit':
        filename = filename[0:dot] + '64' + filename[dot:]
    joblib.dump(attributes, filename, compress=9)
    
def loadClassifier(filename = svmFileName):
    dot = filename.rfind('.')
    if dot == -1:
        dot = len(filename)
    if platform.architecture()[0] == '64bit':
        filename = filename[0:dot] + '64' + filename[dot:]
    return joblib.load(filename)
