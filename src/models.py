'''
Created on 1 Apr 2014



@author: Carlo Meijer, Eduardo Novella
'''

from numpy import *
from pylab import *
import numpy as np
import scipy
#import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn import svm
from sklearn import cross_validation
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
import neurolab as nl
from copy import deepcopy
import matplotlib as plt
from toolbox_02450 import rocplot, confmatplot


############## SVM NEEDS TO BE DONE ON NORMALIZED DATA, VERY IMPORTANT!!! ##################
## Possible problem: need to re-transform back to unnormalized data for actual regression
def svmMultiLinearRegression(trainData, leaveOneOut = True):
    print 'started svm multilinear regression'
    
    #####grab data ##########
    X = np.matrix([problem[1] for problem in trainData])
    nStrategies = len(trainData[0][2])
    
    ##### setup classifiers and their stddevs and means #######
    clfs = []
    stddevs = []
    means = []
    
    ####  scores: prediction of regressor, errors: sum of squared error of regressor   
    scores = []
    errors = []
    
    for i in range(nStrategies):
        if i % 5== 0:
            print 'percent done: ' + str(float(i) / nStrategies * 100)
        y = np.matrix([problem[2][i] for problem in trainData]).T
        
        #### take out all problems it cannot classify
        new_y = []
        new_X = []
        for j in range(len(y.A.ravel())):
            if y[j,0] != -1. :
                new_y.append(y[j,0])
                new_X.append(X[j,:].T)
        new_X= np.asmatrix(squeeze(new_X))
        new_y = np.asmatrix(new_y).T
        
        
        #### set up cross validation, leaveOneOut for actual training  seems best -> save it to file in the Main
        if leaveOneOut:
            CV = cross_validation.LeaveOneOut(len(new_y.A.ravel()))
            K = len(new_y)
        else:
            K = 20
            CV = cross_validation.KFold(len(new_y.A.ravel()),K)
          
        mean_squared_error = 0
        for train_index, test_index in CV:

            # extract training and test set for current CV fold
            X_train, y_train = new_X[train_index,:], new_y[train_index,:]
            X_test, y_test = new_X[test_index,:], new_y[test_index,:]
            
            #train regressor
            clf = svm.SVR(kernel='rbf')
            clf.fit(X_train, y_train.A.ravel())
            #estimate y
            y_test_est = np.mat(clf.predict(X_test)).T
            
            scores.append(mean(y_test_est.A.ravel()[0]))
            
            length = len(y_test.A.ravel())
            mean_squared_error +=(float(sum([(y_test_est.A.ravel()[i] - y_test.A.ravel()[i])**2 for i in range(length)]))/length)
            
        clfs.append(clf.fit(new_X, new_y.A.ravel()))    ##save regressor
        errors.append(mean_squared_error / K)              ##save performance regressor (in sum of squared error)
        
        stddevs.append(std(scores))                     ##save std_dev regressor
        means.append(mean(scores))                      ##save means regressor
    return clfs, errors, means, stddevs
    
def testClassifier(trainData):
    print 'doin svm classification'
    X = np.matrix([problem[1] for problem in trainData])
    nStrategies = len(trainData[0][2])
    clf = DecisionTreeClassifier()
    
    accuracies = []
    for i in range(nStrategies):
        y = np.matrix([problem[2][i] for problem in trainData]).T
        clf.fit(X, y.A.ravel())
        accuracy = clf.score(X,y)
        accuracies.append(accuracy)
    return mean(accuracies)
        #print 'Accuracy for strategy {0}: {1}'.format(i, accuracy)

def pearsonCorrelation(trainData):
    nStrategies = len(trainData[0][2])
    #nSamples = len(trainData)
    #output = np.matrix(nStrategies, nStrategies)
    output = []

    for i in xrange(nStrategies):
        y1 = [problem[2][i] for problem in trainData]
        line = []
        for j in xrange(i):
            line.append(output[j][i])
        for j in xrange(i, nStrategies):
            #if i == j:
                #line.append(1.0)
            #else:
            y2 = [problem[2][j] for problem in trainData]
            correlation = scipy.stats.pearsonr(y1, y2)[0]
            line.append(correlation)
            #output[i,j] = correlation
            #output[j,i] = correlation
        output.append(line)

    return np.matrix(output)

def decisionTreeModel(trainData):
    X = np.matrix([problem[1] for problem in trainData])
    N, M = X.shape

    nStrategies = len(trainData[0][2])
    models = []
    errors = []

    for i in xrange(nStrategies):
        error = 0
        ylist = []
        Xlist = []

        for problem in trainData:
             if problem[2][i] == -1:
                 continue
             ylist.append(problem[2][i])
             Xlist.append(problem[1])

        y = np.matrix(ylist).T
        X = np.matrix(Xlist)

        K = int(np.max([2, np.ceil(float(len(ylist)) / 50)]))
        #K = 10
        #print 'len(y) = {0}, K = {1}'.format(len(ylist), K)
        CV = cross_validation.KFold(len(ylist),K)
        minError=None
        bestModel = None
        configTuple = (0,0)
        for j in xrange(5,10):
            for k in xrange(5,10):
                model = DecisionTreeRegressor(max_depth=j, min_samples_split=k)
                currentError = 0
                for train_index, test_index in CV:
                    X_train, y_train = X[train_index,:], y[train_index,:]
                    X_test, y_test = X[test_index,:], y[test_index,:]
            
                    #fit classifier and predict y
                    model.fit(X_train, y_train.A.ravel())
                    #currentError += (1 - model.score(X_test,y_test))
                    currentError += sum((y_test.A.ravel().tolist() - model.predict(X_test))**2)

                if minError == None or currentError < minError:
                    minError = currentError
                    configTuple = (j,k)
                    bestModel = model

        if i % 5== 0:
            print 'percent done: ' + str(float(i) / nStrategies*100)

        models.append(bestModel.fit(X, ylist))
        errors.append(minError / len(ylist))

    return models, errors

def SVMModel(trainData):
    X = np.matrix([problem[1] for problem in trainData])
    N, M = X.shape

    nStrategies = len(trainData[0][2])
    models = []
    errors = []

    for i in xrange(nStrategies):
        error = 0
        ylist = []
        Xlist = []

        for problem in trainData:
             if problem[2][i] == -1:
                 continue
             ylist.append(problem[2][i])
             Xlist.append(problem[1])

        y = np.matrix(ylist).T
        X = np.matrix(Xlist)

        K = int(np.max([2, np.ceil(float(len(ylist)) / 50)]))
        #K = 10
        #print 'len(y) = {0}, K = {1}'.format(len(ylist), K)
        CV = cross_validation.KFold(len(ylist),K)
        minError=None
        bestModel = None
        configTuple = (0,0)
        for j in xrange(5,10):
            for k in xrange(5,10):
                model = svm.SVR(kernel='rbf')
                currentError = 0
                for train_index, test_index in CV:
                    X_train, y_train = X[train_index,:], y[train_index,:]
                    X_test, y_test = X[test_index,:], y[test_index,:]
            
                    #fit classifier and predict y
                    model.fit(X_train, y_train.A.ravel())
                    #currentError += (1 - model.score(X_test,y_test))
                    currentError += sum((y_test.A.ravel().tolist() - model.predict(X_test))**2)

                if minError == None or currentError < minError:
                    minError = currentError
                    configTuple = (j,k)
                    bestModel = model

        if i % 5== 0:
            print 'percent done: ' + str(float(i) / nStrategies*100)

        models.append(bestModel.fit(X, ylist))
        errors.append(minError / len(ylist))

    return models, errors


def linearRegressionModel(trainData):
    X = np.matrix([problem[1] for problem in trainData])
    N, M = X.shape

    nStrategies = len(trainData[0][2])
    models = []
    errors = []

    for i in xrange(nStrategies):
        error = 0
        ylist = []
        Xlist = []

        for problem in trainData:
             if problem[2][i] == -1:
                 continue
             ylist.append(problem[2][i])
             Xlist.append(problem[1])

        y = np.matrix(ylist).T
        X = np.matrix(Xlist)

        K = int(np.max([2, np.ceil(float(len(ylist)) / 50)]))
        #K = 10
        #print 'len(y) = {0}, K = {1}'.format(len(ylist), K)
        CV = cross_validation.KFold(len(ylist),K)
        model = LinearRegression(fit_intercept=True)
        asd = 0

        for train_index, test_index in CV:
            X_train, y_train = X[train_index,:], y[train_index,:]
            X_test, y_test = X[test_index,:], y[test_index,:]
    
            #fit classifier and predict y
            model.fit(X_train, y_train.A.ravel())
            error += sum((y_test.A.ravel().tolist() - model.predict(X_test))**2)
            asd += len(y_test.A.ravel().tolist())

        if i % 5== 0:
            print 'percent done: ' + str(float(i) / nStrategies*100)

        models.append(model)
        errors.append(error / len(ylist))

    return models, errors


def multiLinearClassificationImproved(trainData):
    # Attempts several values for max_depth and  min_samples_split on a per-strategy basis.
    print 'doin classification'
    X = np.matrix([problem[1] for problem in trainData])
    N, M = X.shape

    nStrategies = len(trainData[0][2])
    clfs = []
    errors = []
    config = []
    performances = []

    for i in xrange(nStrategies):
        minError = None
        configTuple = (0,0)
        bestClf = None
        y = np.matrix([problem[2][i] for problem in trainData]).T
        ylist = y.A.ravel().tolist()

        K = 10
        CV = cross_validation.StratifiedKFold(ylist,K)
        for j in xrange(5,10):
            for k in xrange(5,10):
                clf = DecisionTreeClassifier(max_depth=j, min_samples_split=k)
                currentError = 0
                for train_index, test_index in CV:
                    X_train, y_train = X[train_index,:], y[train_index,:]
                    X_test, y_test = X[test_index,:], y[test_index,:]
            
                    #fit classifier and predict y
                    clf.fit(X_train, y_train.A.ravel())
                    currentError += (1 - clf.score(X_test,y_test))

                if minError == None or currentError < minError:
                    minError = currentError
                    configTuple = (j,k)
                    bestClf = clf

        #clf = DecisionTreeClassifier(max_depth= _max_depth, min_samples_split = _min_samples_split)
        if i % 5== 0:
            print 'percent done: ' + str(float(i) / nStrategies*100)

        performance = float(sum(y)) / len(y)
        clfs.append(bestClf.fit(X, ylist))
        errors.append(minError / K)
        performances.append(performance)
        config.append(configTuple)

    return clfs, errors, config, performances
    
def multiLinearClassification(trainData, _min_samples_split = 2, _max_depth = None): 
    print 'doin classification'
    X = np.matrix([problem[1] for problem in trainData])
    N, M = X.shape
    allErrors = np.matrix#add this to multilinearClassificationImproved if we are going to use that one instead
    nStrategies = len(trainData[0][2])
    clfs = []
    errors = []
    performances = []
    k=0 
    for i in range(nStrategies):
        clf = DecisionTreeClassifier(max_depth= _max_depth, min_samples_split = _min_samples_split)
        if i % 5== 0:
            print 'percent done: ' + str(float(i) / nStrategies*100)
        y = np.matrix([problem[2][i] for problem in trainData]).T
        performance = float(sum(y)) / len(y)
        K = 20
        CV = cross_validation.StratifiedKFold(y.A.ravel().tolist(),K)
        
        this_mean_error = 0
        for train_index, test_index in CV:

            # extract training and test set for current CV fold
            X_train, y_train = X[train_index,:], y[train_index,:]
            X_test, y_test = X[test_index,:], y[test_index,:]
            
            #fit classifier and predict y
            clf.fit(X_train, y_train.A.ravel())
            this_mean_error += (1 - clf.score(X_test,y_test))
                        
        clfs.append(clf.fit(X, y.A.ravel()))
        errors.append(this_mean_error / K)
        performances.append(performance)
    return clfs, errors, performances
    

def linearRegression(trainData, classify=False):
    if classify:
        lr = LogisticRegression(fit_intercept=True)
    else:
        lr = LinearRegression(fit_intercept=True)

    Xtrain = np.matrix([problem[1] for problem in trainData])
    output = []
    nStrategies = len(trainData[0][2])
    for i in xrange(nStrategies):
        # single column of strategy times
        if classify:
            ytrain = [problem[2][i] for problem in trainData]
        else:
            ytrain = np.matrix([problem[2][i] for problem in trainData]).T
        # attempt to fit values in this column given the features
        lr.fit(Xtrain, ytrain)

        # append coefficients into output so we can predict later
        if classify:
            output.append(deepcopy(lr))
        else:
            output.append([float(lr.intercept_)] + [lr.coef_[0][j] for j in xrange(lr.coef_.shape[1])])
        #print 'w0 : {0}\nwx : {1}'.format(lr.intercept_, lr.coef_)
        #if i > 10:
             #break;
    if classify:
        return output
    else:
        return np.matrix(output)

def linearRegressionPredict(testData, model, classify=False): # model = output from linearRegression(trainData)
    if classify:
        #lr = LogisticRegression(fit_intercept=True)
        predict = []

        nStrategies = len(model)
        for i in xrange(nStrategies):
            lr = model[i]

            Xtest = np.matrix([problem[1] for problem in testData])
            tmpList = lr.predict(Xtest)

            predict.append(tmpList)

    else:
        lr = LinearRegression(fit_intercept=True)

        predict = []
        (nStrategies, nCoef) = model.shape
        for i in xrange(nStrategies):
            lr.intercept_ = model[i,0]
            lr.coef_ = np.matrix([model[i,j] for j in xrange(1, nCoef)])

            Xtest = np.matrix([problem[1] for problem in testData])
            tmpMatrix = lr.predict(Xtest)
            tmpList = [tmpMatrix[i,0] for i in xrange(tmpMatrix.shape[0])]
            predict.append(tmpList)
    
    return np.matrix(predict).T

def linearRegressionError(trainData, prediction): # prediction = output from linearRegressionPredict
    strategies = np.matrix([problem[2] for problem in trainData])
    (nProblems, nStrategies) = strategies.shape
    output = []

    for i in xrange(nStrategies):
        actual  = strategies[:,i]
        predict = prediction[:,i]

        output.append(np.abs((actual - predict)).sum().astype(float) / nProblems)

    return output
    
def strategyPruning(tuples):
    
    
    
    
    return acceptedStrategies