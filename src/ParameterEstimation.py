'''
Created on 7 Apr 2014

@author: Stef Janssen
'''

from __future__ import print_function

from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
import numpy as np

#print(__doc__)


def runEstimation(trainData):
    X = np.matrix([problem[1] for problem in trainData])
    nStrategies = len(trainData[0][2])
    nStrategies = 1
    for i in range(nStrategies):
        y = np.matrix([problem[2][i] for problem in trainData]).T
         #### take out all problems it cannot classify
        new_y = []
        new_X = []
        for j in range(len(y.A.ravel())):
            if y[j,0] != -1. :
               
                new_y.append(y[j,0])
                new_X.append(X[j,:].T)
        new_X= np.asarray(np.asmatrix(np.squeeze(new_X)))
        new_y = np.asarray(np.asmatrix(new_y).T)
        estimateParameters(new_X, new_y)


def estimateParameters(X, y):
    
    # Split the dataset in two equal parts
    print (np.asarray(X))
    print (np.squeeze(np.asarray(y)))
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.5, random_state=0)
    
    # Set the parameters by cross-validation
    tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                         'C': [1, 10, 100, 1000]},
                        {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
    
    scores = ['precision', 'recall']
    
    for score in scores:
        print("# Tuning hyper-parameters for %s" % score)
        print()
    
        clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5, scoring=score)
        clf.fit(X_train, y_train)
    
        print("Best parameters set found on development set:")
        print()
        print(clf.best_estimator_)
        print()
        print("Grid scores on development set:")
        print()
        for params, mean_score, scores in clf.grid_scores_:
            print("%0.3f (+/-%0.03f) for %r"
                  % (mean_score, scores.std() / 2, params))
        print()
    
        print("Detailed classification report:")
        print()
        print("The model is trained on the full development set.")
        print("The scores are computed on the full evaluation set.")
        print()
        y_true, y_pred = y_test, clf.predict(X_test)
        print(classification_report(y_true, y_pred))
        print()

# Note the problem is too easy: the hyperparameter plateau is too flat and the
# output model is the same for precision and recall with ties in quality.